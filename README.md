# FireFox Youtube Redirect
A simple extension to help consume YouTube videos without tracking nor ads through the [Invidous](https://invidious.io/) project.

## But why?
Since the adoption for using a more privacy centric medium for YouTube consumption, I have found it painsakingly annoying when I have to personally mangle the URL of a YouTube link for redirection.

FireFoxYoutubeRedirect is a personal project intended to solve my and limited others' very niche headache.

## Development
### Testing
#### Running non ui unit tests
run the following to run all tests that are not user facing
```bash
npm run test
```

#### Running UI unit tests
run the following to run all UI unit tests
```bash
npm run test:ui
```

#### Manual Testing
run the following to build the application.
```bash
npm run build
```

navigate to [FireFox debugging](about:debugging#/runtime/this-firefox) and load in a temporary add-on
select `manifest.json` in `./build`

## Signing the addon
run the following to create a `.xpi` compressed file with Mozilla signiture. This is required for side loading the application without going through the addon store.
```bash
npm run sign
```

## Creating a compressed file
run the following to create a `.zip` file in `dist` based on the contents of the `build` folder
```bash
npm run zip
```
