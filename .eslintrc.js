// next line is disabled as this is the only option in js files
const fs = require("fs"); // eslint-disable-line 

const configFiles = fs
  .readdirSync('./eslintrc')
  .map(file => `./eslintrc/${file}`);

module.exports = {
  extends: [...configFiles],
  parserOptions: {
    project: ['./tsconfig.json', './tsconfig.eslint.json'],
  },
  overrides: [{
    files: ['*.test.ts'],
    rules: {
      'no-new': 'off',
    },
  }],
};
