export default interface CrudServices {
  get: <T>() => Promise<T>;
}

