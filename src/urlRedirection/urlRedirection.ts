export default function urlRedirectionFactory() {
  if (urlRedirectionService === null) {
    urlRedirectionService = new ConcreteUrlRedirection();

    return urlRedirectionService;
  }

  return urlRedirectionService;
}

let urlRedirectionService: UrlRedirection | null = null;

class ConcreteUrlRedirection implements UrlRedirection {
  #isRedirectionEnabled = true;
  #destinationUrl: string = '';

  initListeners() {
    this.#initDisableRedirectListener();
    this.#initRedirectListener();
  }

  setDestinationUrl(newTargetUrl: string): void {
    this.#destinationUrl = newTargetUrl;
  }

  #initDisableRedirectListener() {
    browser.runtime.onMessage.addListener(
      (incomingStatus: boolean | null, sender, sendResponse) => {
        const tempStatus = incomingStatus ?? this.#isRedirectionEnabled;
        const responseDto: ToUiDisableRedirectionDto = {
          curRedirectionStatus: tempStatus,
        }
        this.#isRedirectionEnabled = tempStatus;
        // eslint-disable-next-line no-unused-expressions
        tempStatus ? this.#initRedirectListener() : this.#removeRedirectListener();

        sendResponse(responseDto);
      },
    )
  }

  #initRedirectListener(): void {
    browser.webRequest.onBeforeRequest.addListener(
      this.#redirectionListener,
      { urls: ['*://*.youtube.com/*'] },
      ['blocking'],
    )
  }

  #removeRedirectListener(): void {
    browser.webRequest.onBeforeRequest.removeListener(this.#redirectionListener)
  }

  #redirectionListener = (details: browser.webRequest._OnBeforeRequestDetails) => {
    const { pathname, search } = new URL(details.url);
    const redirectionUrl = new URL(
      pathname + search,
      this.#destinationUrl,
    ).toString();

    return {
      redirectUrl: redirectionUrl,
    }
  }
}

export interface ToUiDisableRedirectionDto {
  curRedirectionStatus: boolean;
}

interface UrlRedirection {
  initListeners: () => void;
  setDestinationUrl: (newDestinationUrl: string) => void;
}
