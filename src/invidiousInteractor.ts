import AxiosService from './axiosService/axiosService';
import { InstanceMapper, InstanceSelector } from './instanceServices';
import ConcreteInvidiousInstancesService from './invidiousInstancesService';

export default class InvidiousInteractor {
  #instanceSelector = new InstanceSelector();
  #instanceMapper = new InstanceMapper();
  #invidiousInstancesService = new ConcreteInvidiousInstancesService(
    new AxiosService('https://api.invidious.io/instances.json'),
  );

  async getUrl(): Promise<string> {
    const instances = await this.#invidiousInstancesService.getInstances();

    return this.#instanceSelector.getBestInstanceUrl(this.#instanceMapper.map(instances))
  }
}
