import { ExceptionCodes } from '../baseException';
import CrudServices from '../crudServices';
import { InvidiousInstancesApiDto, RawInvidiousInstance } from './invidiousInstancesDto';
import InvidiousInstancesError from './invidiousInstancesError';

export default class ConcreteInvidiousInstancesService implements InvidiousInstancesService {
  #crudServices: CrudServices;

  constructor(crudServices: CrudServices) {
    this.#crudServices = crudServices;
  }

  async getInstances(): Promise<RawInvidiousInstance[]> {
    try {
      const response = await this.#crudServices.get<InvidiousInstancesApiDto>();

      return response.data;
    } catch (error) {
      throw new InvidiousInstancesError(
        ExceptionCodes.OPERATION_VIOLATION,
        `ConcreteInvidiousInstancesService getInstances failed: errored out with ${error}`,
      )
    }
  }
}

export interface InvidiousInstancesService {
  getInstances: () => Promise<RawInvidiousInstance[]>;
}
