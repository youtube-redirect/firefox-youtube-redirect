import BaseException from '../baseException';

export default class InvidiousInstancesError extends BaseException {}
