import { MockAxiosService } from '../axiosService';
import ConcreteInvidiousInstancesService from './invidiousInstancesService';

describe('InvidiousInstanceService', () => {
  describe('getInstances', () => {
    it('throws an error with correct message', async () => {
      const invidiousInstancesService = new ConcreteInvidiousInstancesService(new MockAxiosService());

      await expect(invidiousInstancesService.getInstances()).rejects.toThrow();
    });
  });
})
