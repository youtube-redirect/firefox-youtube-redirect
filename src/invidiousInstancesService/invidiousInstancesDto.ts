export interface InvidiousInstancesApiDto {
  data: RawInvidiousInstance[];
}

export interface InstanceMonitor {
  token: string;
  url: null | string;
  alias: string;
  last_status: number;
  uptime: number;
  down: boolean;
  down_since: null | string;
  up_since: string;
  error: null | string;
  period: number;
  apdex_t: number;
  string_match: string;
  enabled: boolean;
  published: boolean;
  disabled_locations: string[];
  recipients: string[];
  last_check_at: string;
  next_check_at: string;
  created_at: string;
  mute_until: string;
  favicon_url: string;
  custom_headers: Record<string | number | symbol, unknown>;
  http_verb: string;
  http_body: string;
  ssl: {
    tested_at: string;
    expires_at: string;
    valid: boolean;
    error: null | string;
  };
}

export type RawInvidiousInstance = [
  string,
  {
    flag: string;
    region: string;
    stats: InstanceStats | null;
    cors: boolean | null;
    api: boolean | null;
    type: string;
    uri: string;
    monitor: InstanceMonitor | null;
  },
]

interface InstanceStats {
  version: string;
  software: {
    name: string;
    version: string;
    branch: string;
  };
  openRegistrations: boolean;
  usage: {
    users: {
      total: number;
      activeHalfYear: number;
      activeMonth: number;
    };
  };
  metadata: {
    updatedAt: number;
    lastChannelRefreshedAt: number;
  };
  playback: {
    totalRequests?: number;
    successfulRequests?: number;
    ratio?: number;
  };
}

