import { MockInstanceMapper } from '../instanceMapper';
import ConcreteInstanceSelector from './instanceSelector';
import InstanceSelectorError from './instanceSelectorError';

describe('ConcreteInstanceSelector', () => {
  describe('getBestInstanceUrl', () => {
    it('throws an error when no instances were provided', () => {
      expect(() => new ConcreteInstanceSelector().getBestInstanceUrl([]))
        .toThrow(InstanceSelectorError);
    });

    it('returns the expect url', () => {
      const instances = new MockInstanceMapper().map();
      const [{ url }] = instances.sort((a, b) => b.healthRating - a.healthRating);

      expect(new ConcreteInstanceSelector().getBestInstanceUrl(instances)).toStrictEqual(url);
    })
  })
})
