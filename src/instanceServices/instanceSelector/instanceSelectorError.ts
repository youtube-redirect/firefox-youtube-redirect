import BaseException from '../../baseException/baseException';

export default class InstanceSelectorError extends BaseException {}
