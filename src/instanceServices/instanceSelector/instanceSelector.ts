import { ExceptionCodes } from '../../baseException';
import { CleansedInvidiousInstances } from '../instanceMapper';
import InstanceSelectorError from './instanceSelectorError';

export default class ConcreteInstanceSelector implements InstanceSelector {
  getBestInstanceUrl(cleansedInstances: CleansedInvidiousInstances[]): string {
    if (cleansedInstances.length < 1) {
      throw new InstanceSelectorError(
        ExceptionCodes.OPERATION_VIOLATION,
        'ConcreteInstanceSelector getBestInstanceUrl failed: no instances were found.',
      )
    }

    const instances = cleansedInstances.map(instance => ({
      url: instance.url,
      healthRating: instance.healthRating,
    })).sort((a, b) => b.healthRating - a.healthRating)

    return instances[0].url;
  }
}

interface InstanceSelector {
  getBestInstanceUrl: (cleansedInstances: CleansedInvidiousInstances[]) => string;
}

