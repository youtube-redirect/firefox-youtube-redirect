import { faker } from '@faker-js/faker';
import { CleansedInvidiousInstances, InstanceMapper } from './instanceMapper';

export default class MockInstanceMapper implements InstanceMapper {
  map(): CleansedInvidiousInstances[] {
    return [
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
      {
        url: faker.internet.url(),
        healthRating: faker.number.float({ fractionDigits: 2 }),
      },
    ]
  }
}
