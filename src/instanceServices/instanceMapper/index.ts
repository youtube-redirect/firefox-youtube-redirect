export { default } from './instanceMapper';
export type { CleansedInvidiousInstances } from './instanceMapper';
export { default as MockInstanceMapper } from './instanceMapperMock';
