import {
  InstanceMonitor,
  RawInvidiousInstance,
} from '../../invidiousInstancesService';

export default class ConcreteInstanceMapper implements InstanceMapper {
  map(data: RawInvidiousInstance[]): CleansedInvidiousInstances[] {
    if (data.length === 0) {
      return [];
    }

    return data
      .filter(data => data[1].uri && data[1].monitor)
      .map(data => {
        const [, { uri, monitor }] = data;
        const { uptime } = monitor as InstanceMonitor;

        return {
          url: uri,
          healthRating: uptime,
        }
      });
  }
}

export interface InstanceMapper {
  map: (data: RawInvidiousInstance[]) => CleansedInvidiousInstances[];
}

export interface CleansedInvidiousInstances {
  url: string;
  healthRating: number;
}

