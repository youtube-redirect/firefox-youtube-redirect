import { MockInvidiousInstancesService } from '../../invidiousInstancesService';
import ConcreteInstanceMapper from './instanceMapper';

describe('InstanceMapper', () => {
  describe('map', () => {
    it('returns the correctly mapped data', async () => {
      const instances = await new MockInvidiousInstancesService().getInstances();
      const mappedInstances = new ConcreteInstanceMapper().map(instances);

      expect(mappedInstances[0]).toHaveProperty('url');
      expect(mappedInstances[0]).toHaveProperty('healthRating');
    });
  })
})
