export default class BaseException extends Error implements Exception {
  #statusCode: ExceptionCodes;

  constructor(statusCode: ExceptionCodes, message?: string) {
    super(message);
    this.#statusCode = statusCode;
  }

  get statusCode() { return this.#statusCode }
}

interface Exception {
  readonly statusCode: ExceptionCodes;
}

export enum ExceptionCodes {
  DATA_INTEGRITY_VIOLATION = 'DATA_INTEGRITY_VIOLATION',
  NOT_IMPLEMENTED = 'NOT_IMPLEMENTED',
  NOT_EXIST = 'NOT_EXIST',
  OPERATION_VIOLATION = 'OPERATION_VIOLATION',
}
