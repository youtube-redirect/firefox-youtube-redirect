import InvidiousInteractor from './invidiousInteractor';
import urlRedirectionFactory from './urlRedirection';

class ConcreteBackgroundScript implements BackgroundScript {
  #invidiousInteractor = new InvidiousInteractor();
  #redirect = urlRedirectionFactory();

  constructor() {
    (async () => {
      this.#redirect.setDestinationUrl(await this.#invidiousInteractor.getUrl());
      this.#redirect.initListeners();
    })().catch(error => console.log(error));
  }

  init() {
    this.#initiateSetInterval();
  }

  #initiateSetInterval() {
    setInterval(() => {
      (async () => {
        this.#redirect.setDestinationUrl(await this.#invidiousInteractor.getUrl());
      })().catch(error => console.log(error));
    }, 600000);
  }
}

interface BackgroundScript {
  init: () => void;
}

new ConcreteBackgroundScript().init()
