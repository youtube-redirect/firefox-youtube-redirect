import BaseException from '../baseException/baseException';

export default class AxiosServiceError extends BaseException {}
