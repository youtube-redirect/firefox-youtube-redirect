import axios from 'axios';
import { ExceptionCodes } from '../baseException';
import CrudServices from '../crudServices/crudServices';
import AxiosServiceError from './axiosServiceError';

export default class AxiosService implements CrudServices {
  #url: string;

  constructor(url: string) {
    this.#url = url;
  }

  async get<T>(): Promise<T> {
    try {
      return await axios({
        method: 'get',
        url: this.#url,
      })
    } catch (error) {
      throw new AxiosServiceError(
        ExceptionCodes.OPERATION_VIOLATION,
        `AxiosService get failed: get failed with ${error}`,
      )
    }
  }
}
