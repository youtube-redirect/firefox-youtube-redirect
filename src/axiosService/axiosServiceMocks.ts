import { ExceptionCodes } from '../baseException';
import CrudServices from '../crudServices/crudServices';
import AxiosServiceError from './axiosServiceError';

export default class MockAxiosService implements CrudServices {
  async get<T>(): Promise<T> {
    throw new AxiosServiceError(
      ExceptionCodes.OPERATION_VIOLATION,
      `MockAxiosService: throwing error`,
    )
  }
}
