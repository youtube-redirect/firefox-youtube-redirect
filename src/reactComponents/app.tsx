import { useEffect, useState } from 'react';
import { ToUiDisableRedirectionDto } from '../urlRedirection';
import DisableRedirectionButton from './disableRedirectionButton';

export default function App() {
  const [redirectionState, setRedirectionState] = useState(useSetBgDisableRedirect().bgScriptDisableRedirectionStatus!);
  const { bgScriptDisableRedirectionStatus, operationalState } = useSetBgDisableRedirect(redirectionState);

  return (
    <div className="text-white p-4 bg-gradient-to-r from-purple to-dark-purple" >
      <div className="bg-gradient-to-r from-lavendar to-dark-lavendar rounded-lg p-4">
        <h1 className="text-3xl font-bold underline">
          !YouTube
        </h1>
        <p>
          Helping the adoption of a safer and more private Internet.
        </p>

        <DisableRedirectionButton
          disableStatus={bgScriptDisableRedirectionStatus}
          operationalState={operationalState}
          setRedirectionState={setRedirectionState}
        />
      </div>
    </div>
  );
}

export enum OperationalState {
  SUCCESS = 'fulfilled',
  INPROGRESS = 'pending',
  ERROR = 'error',
}

function useSetBgDisableRedirect(newStatus: boolean | null = null) {
  const [disableRedirectionStatus, setDisableRedirectionStatus] = useState<DisableRedirectionStatus>({
    bgScriptDisableRedirectionStatus: null,
    operationalState: OperationalState.INPROGRESS,
  });

  useEffect(() => {
    (async () => {
      const { curRedirectionStatus } = await browser.runtime.sendMessage(newStatus) as ToUiDisableRedirectionDto;

      setDisableRedirectionStatus({
        bgScriptDisableRedirectionStatus: curRedirectionStatus,
        operationalState: OperationalState.SUCCESS,
      })
    })().catch(() => setDisableRedirectionStatus({
      bgScriptDisableRedirectionStatus: null,
      operationalState: OperationalState.ERROR,
    }));
  }, [newStatus]);

  return disableRedirectionStatus;
}

interface DisableRedirectionStatus {
  bgScriptDisableRedirectionStatus: boolean | null;
  operationalState: OperationalState;
}
