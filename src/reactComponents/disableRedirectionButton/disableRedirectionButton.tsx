import { OperationalState } from '../app';

export default function DisableRedirectionButton(
  { disableStatus, operationalState, setRedirectionState }: DisableRedirectionButtonProps,
) {
  return (
    <>
      { disableStatus !== null && operationalState === OperationalState.SUCCESS
        && <div className="flex items-center">
          <p className="mt-1 text-white">Redirection</p>
          <label className="relative cursor-pointer mx-2">
            <input
              type="checkbox"
              className="sr-only peer"
              checked={disableStatus}
              onChange={() => setRedirectionState(!disableStatus)}
            />
            <div
              className="w-[53px] h-7 flex items-center bg-gray-500 rounded-full text-[9px]
                peer-checked:text-lavendar text-gray-500 font-extrabold after:flex after:items-center
                after:justify-center peer after:content-['Off'] peer-checked:after:content-['On']
                peer-checked:after:translate-x-full after:absolute after:left-[2px]
                peer-checked:after:border-white after:bg-white after:border after:border-gray-300
                after:rounded-full after:h-6 after:w-6 after:transition-all peer-checked:bg-[#fe8966]"
            >
            </div>
          </label>
        </div>
      }

      { operationalState === OperationalState.INPROGRESS
        && <div>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="w-10 animate-spin fill-white block mx-auto"
            viewBox="0 0 24 24">
            <title>spinner</title>
            <path
              d="M12 22c5.421 0 10-4.579 10-10h-2c0 4.337-3.663 8-8
              8s-8-3.663-8-8c0-4.336 3.663-8 8-8V2C6.579 2 2 6.58
              2 12c0 5.421 4.579 10 10 10z"
              data-original="#000000" />
          </svg>
        </div>
      }
    </>
  )
}

interface DisableRedirectionButtonProps {
  disableStatus: boolean | null;
  operationalState: OperationalState;
  setRedirectionState: React.Dispatch<React.SetStateAction<boolean>>;
}
