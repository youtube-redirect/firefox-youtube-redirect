import { fireEvent, render, screen } from '@testing-library/react';
import { OperationalState } from '../app';
import DisableRedirectionButton from './disableRedirectionButton';

describe('DisableRedirectionButton', () => {
  describe('on successful operational state', () => {
    it('redirection toggle is checked when redirection is on', () => {
      render(<DisableRedirectionButton
        disableStatus={true}
        operationalState={OperationalState.SUCCESS}
        setRedirectionState={jest.fn()}
      />)

      expect(screen.getByRole('checkbox')).toBeChecked();
    })

    it('redirection toggle is not checked when redirection is off', () => {
      render(<DisableRedirectionButton
        disableStatus={false}
        operationalState={OperationalState.SUCCESS}
        setRedirectionState={jest.fn()}
      />)

      expect(screen.getByRole('checkbox')).not.toBeChecked();
    })

    it('clicking on the toggle button sets the expected value', () => {
      const mockSetFunction = jest.fn();
      const mockDisableStatus = false;

      render(<DisableRedirectionButton
        disableStatus={mockDisableStatus}
        operationalState={OperationalState.SUCCESS}
        setRedirectionState={mockSetFunction}
      />)

      fireEvent.click(screen.getByRole('checkbox'));

      expect(mockSetFunction).toHaveBeenCalledTimes(1);
      expect(mockSetFunction).toHaveBeenCalledWith(!mockDisableStatus);
    })
  })

  describe('on pending operational state', () => {
    it('renders a spinner', () => {
      render(<DisableRedirectionButton
        disableStatus={true}
        operationalState={OperationalState.INPROGRESS}
        setRedirectionState={jest.fn()}
      />)

      expect(screen.getByTitle('spinner')).toBeInTheDocument();
    })
  })
})
