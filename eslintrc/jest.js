module.exports = {
  env: {
    jest: true,
  },
  extends: [
    'plugin:jest/recommended',
    'plugin:jest/style',
  ],
  rules: {
    'jest/consistent-test-it': 'error',
    'jest/expect-expect': 'off',
    'jest/prefer-lowercase-title': ['error', { ignore: ['describe'] }],
    'jest/no-deprecated-functions': 'error',
    'jest/no-duplicate-hooks': 'error',
    'jest/no-jasmine-globals': 'error',
    'jest/no-standalone-expect': 'error',
    'jest/no-test-return-statement': 'error',
    'jest/prefer-spy-on': 'error',
    'jest/prefer-strict-equal': 'error',
    'jest/prefer-todo': 'warn',
    'jest/require-top-level-describe': 'error',
    'jest/valid-title': 'error',
  },
  overrides: [
    {
      files: '*',
      excludedFiles: ['*.test.ts', '*.spec.ts', '*.test.tsx', '*.spec.tsx'],
      rules: {
        'jest/no-export': 'off',
        'jest/require-top-level-describe': 'off',
      },
    },
  ],
};
