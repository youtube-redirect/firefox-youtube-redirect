#!/bin/sh

set -euo pipefail

read -p "Enter Api Key: " apiKey
read -p "Enter Api Secret: " apiSecret

rm -r dist || true

mkdir dist
npx web-ext --api-key $apiKey --api-secret $apiSecret -a ./dist -s ./build sign && mv ./build/.web-extension-id ./dist

