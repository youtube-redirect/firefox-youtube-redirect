#!/bin/sh

set -euo pipefail

npm run build
rm -r ./dist || true && mkdir ./dist
pushd ./build
zip -r ../dist/firefox-youtube-redirect.zip ./*
popd
