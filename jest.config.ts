import type { Config } from 'jest';

const isUiTesting = process.env.UI === 'true';

const config: Config = {
  roots: ["<rootDir>"],
  transform: {
    ".(ts|tsx)": "ts-jest",
  },
  setupFilesAfterEnv: isUiTesting ? ["<rootDir>/jest.setup.ts"] : [],
  testEnvironment: isUiTesting ? "jsdom" : "node",
  testRegex: isUiTesting ? ".*.(test|spec).(tsx)$" : ".*.(test|spec).(ts)$",
  testPathIgnorePatterns: [
    "/node_modules/",
    ".*.snapshots.ts",
    "/dist/",
  ],
};

export default config;

