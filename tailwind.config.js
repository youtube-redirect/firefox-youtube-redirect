/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        'dark-purple': '#14122d',
        purple: '#201c40',
        'light-purple': '#24204b',
        lavendar: '#6f4bf8',
        'dark-lavendar': '#5130cf',
      },
    },
  },
  plugins: [],
}

