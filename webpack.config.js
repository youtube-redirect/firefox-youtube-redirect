const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    main: './src/background-script.ts',
  },
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'background-script.js',
    chunkFilename: '[name].chunk.js',
  },
  resolve: {
    extensions: ['.ts', '.js'],
  },
  module: {
    rules: [
      {
        test: /\.ts?$/,
        include: path.resolve(__dirname, './src'),
        loader: 'ts-loader',
      },
    ],
  },
};
